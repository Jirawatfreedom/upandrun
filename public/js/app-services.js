function ItemService(opt_items) {
  var items = opt_items || [];
  this.list = function() {
    return items;
  };
  this.add = function(item) {
    items.push(item);
  };
}
angular.module('notesApp', [])
  .provider('ItemService', function() {
    var haveDefaultItems = true;
    this.disableDefaultItems = function() {
      haveDefaultItems = false;
    };
    //this function gets our dependencies. not the provider above
    this.$get = [function() {
      var optItems = [];
      if (haveDefaultItems) {
        optItems = [{
          id: 1,
          label: 'Item 0'
        }, {
          id: 2,
          label: 'Item 1'
        }, ];
      }
      return new ItemService(optItems);
    }];

  })
  .config(['ItemServiceProvider',
    function(ItemServiceProvider) {
      //To see how the provider can chnage configuration change the value of shouldHaveDefault to ture and try running the example
    //  var shouldHaveDefaults = false;//show 0 item
      var shouldHaveDefaults = true;//show 2 item
      //Get configufation from server
      //Set shouldHaveDefaults soomehow
      //Assume it magically change for now
      if (!shouldHaveDefaults) {
        ItemServiceProvider.disableDefaultItems();
      }
    }
  ])
  .controller('MainCtrl', [function() {
    var self = this;
    self.tab = 'first';
    self.open = function(tab) {
      self.tab = tab;
    };
  }])
  .controller('SubCtrl', ['ItemService', function(ItemService) {
    var self = this;
    self.list = function() {
      return ItemService.list();
    };
    self.add = function() {
      ItemService.add({
        id: self.list().length + 1,
        label: 'Item ' + self.list().length
      });
    };
  }]);

/* use Class OO programming
function ItemService() {
  var items = [{
    id: 1,
    label: 'Item 0'
  }, {
    id: 2,
    label: 'Item 1'
  }, ];
  this.list = function() {
    return items;
  };
  this.add = function(item) {
    items.push(item);
  }
}
angular.module('notesApp', [])
  .service('ItemService', [ItemService])
  .controller('MainCtrl', [function() {
    var self = this;
    self.tab = 'first';
    self.open = function(tab) {
      self.tab = tab;
    };
  }])
.controller('SubCtrl', ['ItemService', function(ItemService) {
  var self = this;
  self.list = function() {
    return ItemService.list();
  };
  self.add = function() {
    ItemService.add({
      id: self.list().length + 1,
      label: 'Item ' + self.list().length
    });
  };
}]);
*/
/* Version fatory
angular.module('notesApp',[])
.controller('MainCtrl',[function(){
  var self =this;
  self.tab = 'first';
  self.open = function(tab){
    self.tab = tab;
  };
}])
.controller('SubCtrl',['ItemService',function(ItemService){
  var self = this;
  self.list = function (){
    return ItemService.list();
  };
  self.add = function(){
    ItemService.add({
      id: self.list().length + 1,
      label: 'Item ' + self.list().length
    });
  };
}])
.factory('ItemService', [function(){
  var items = [
    {id : 1 , label: 'Item 0'},
    {id : 2 , label: 'Item 1'},
  ];
  return {
    list: function(){
      return items;
    },
    add : function(item){
      items.push(item);
    }
  };
}]);
*/
